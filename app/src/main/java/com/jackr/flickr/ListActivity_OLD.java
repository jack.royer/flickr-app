package com.jackr.flickr;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.ImageRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

public class ListActivity_OLD extends AppCompatActivity {

  private static class MyAdapter extends BaseAdapter {

    Vector<String> items;

    MyAdapter() {
      this.items = new Vector<>();
    }

    public void add(String item) {
      items.add(item);
    }

    @Override
    public int getCount() {
      return items.size();
    }

    @Override
    public Object getItem(int position) {
      return items.get(position);
    }

    @Override
    public long getItemId(int position) {
      return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // TODO add some recycling
      LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      // convertView = li.inflate(R.layout.textviewlayout, parent, false);
      // TextView textView = convertView.findViewById(R.id.textView);
      // textView.setText(items.get(position));

      convertView = li.inflate(R.layout.bitmaplayout, parent, false);

      View finalConvertView = convertView;

      ImageRequest imageRequest = new ImageRequest(items.get(position), (bitmap) -> {
        Log.i("Jackr", "Received bitmap");
        ImageView imageView = finalConvertView.findViewById(R.id.imageView2);
        imageView.setImageBitmap(bitmap);
      }, 0, 0, null, null);

      SingletonQueue.getInstance(parent.getContext()).addToRequestQueue(imageRequest);

      return convertView;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);

    ListView list = findViewById(R.id.list);
    list.setAdapter(new MyAdapter());

    new AsyncFlickrJSONData((json) -> {
      try {
        JSONArray items = json.getJSONArray("items");

        MyAdapter adapter = (MyAdapter) list.getAdapter();

        SingletonQueue.getInstance(this.getApplicationContext()).getRequestQueue();

        for (int i = 0; i < items.length(); i++) {
          JSONObject item = items.getJSONObject(i);
          String link = item.getJSONObject("media").getString("m");

          adapter.add(link);
          Log.i("Jackr", "Adding to adapter url : " + link);
        }

        adapter.notifyDataSetChanged();

      } catch (JSONException e) {
        e.printStackTrace();
      }

    }).execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
  }
}
