package com.jackr.flickr;

import android.util.Log;

import java.util.Arrays;
import java.util.Vector;

public class Card {
  public String media_url;
  public String title;
  public Vector<String> tags;

  public Card(String media_url, String title) {
    this.media_url = media_url;
    this.title = title;
    this.tags = new Vector<>();

    Log.d("Card", "Created card: " + this.title);
  }

  public void setTags(String tagString) {
    String[] tags = tagString.split(" ");
    this.tags.addAll(Arrays.asList(tags));
  }
}
