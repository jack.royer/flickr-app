package com.jackr.flickr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Consumer;

public class AsyncDownloadBitmap extends AsyncTask<String, Void, Bitmap> {

  private final Consumer<Bitmap> then;

  AsyncDownloadBitmap(Consumer<Bitmap> then) {
    this.then = then;
  }

  @Override
  protected Bitmap doInBackground(String... strings) {
    try {
      URL url = new URL(strings[0]);
      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
      try {
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        return BitmapFactory.decodeStream(in);
      } finally {
        urlConnection.disconnect();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  protected void onPostExecute(Bitmap bitmap) {
    this.then.accept(bitmap);
  }
}
