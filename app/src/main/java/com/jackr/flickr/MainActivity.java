package com.jackr.flickr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    // Add getImageOnClickListener() to the onClick() method of the button
    Button buttonGetImage = findViewById(R.id.buttonGetImage);
    buttonGetImage.setOnClickListener((e) -> getImageOnClickListener());

    // Switch to the ListActivity
    Intent intent = new Intent(this, ListActivity.class);
    startActivity(intent);
  }

  private void getImageOnClickListener() {
    new AsyncFlickrJSONData(
        (json) -> {
          try {
            JSONArray items = json.getJSONArray("items");

            // chooses a random item from the list
            int random = (int) (Math.random() * items.length());
            JSONObject item = items.getJSONObject(random);

            String title = item.getString("title");
            String link = item.getJSONObject("media").getString("m");

            // download the image
            AsyncDownloadBitmap asyncFlickrImage = new AsyncDownloadBitmap((bitmap) -> {
              // scales the image to the size of the imageView
              ImageView imageView = findViewById(R.id.imageView);

              float aspectRatio = bitmap.getWidth() /
                  (float) bitmap.getHeight();

              if (aspectRatio > 1) {
                int width = imageView.getWidth();
                Log.i("MainActivity", "width: " + width);
                int height = Math.round(width / aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                    bitmap, width, height, false);
              } else {
                int height = imageView.getHeight();
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                    bitmap, width, height, false);
              }

              // display the image
              imageView.setImageBitmap(bitmap);
            });

            asyncFlickrImage.execute(link);

            // display the title
            TextView titleView = findViewById(R.id.textViewTitle);
            titleView.setText(title);


          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
    ).execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");

  }
}
