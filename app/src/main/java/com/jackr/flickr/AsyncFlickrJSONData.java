package com.jackr.flickr;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Consumer;

public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

  private final Consumer<JSONObject> then;

  private String readStream(InputStream is) {
    try {
      ByteArrayOutputStream bo = new ByteArrayOutputStream();
      int i = is.read();
      while (i != -1) {
        bo.write(i);
        i = is.read();
      }
      return bo.toString();
    } catch (IOException e) {
      return "";
    }
  }

  AsyncFlickrJSONData(Consumer<JSONObject> then) {
    this.then = then;
  }

  @Override
  protected JSONObject doInBackground(String... strings) {
    try {
      URL url = new URL(strings[0]);
      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
      try {
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        String s = readStream(in);
        JSONObject json = new JSONObject(s.substring(s.indexOf("{"), s.lastIndexOf("}") + 1));

        return json;
      } finally {
        urlConnection.disconnect();
      }
    } catch (IOException | JSONException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  protected void onPostExecute(JSONObject jsonObject) {
    // Logs the JSONObject
    Log.i("AsyncFlickrJSONData", jsonObject.toString());

    // Calls the Consumer
    then.accept(jsonObject);
  }
}
