package com.jackr.flickr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.OnTokenCanceledListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

public class ListActivity extends AppCompatActivity {

  private FusedLocationProviderClient fusedLocationClient;

  SharedPreferences.OnSharedPreferenceChangeListener listener;

  private class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> {
    Vector<String> tags;

    TagAdapter(Vector<String> tags) {
      this.tags = tags;
      if (this.tags.size() > 3) {
        this.tags.subList(3, this.tags.size()).clear();
      }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      private final Button button;

      public ViewHolder(View view) {
        super(view);
        button = view.findViewById(R.id.tagButton);
      }

      public Button getButton() {
        return button;
      }
    }


    @NonNull
    @Override
    public TagAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.buttonlayout, parent, false);

      return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull TagAdapter.ViewHolder holder, int position) {
      Button button = holder.getButton();
      button.setText(tags.get(position));
      button.setOnClickListener(v -> {
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putString("tag", tags.get(position));
        myEdit.apply();
      });

    }

    @Override
    public int getItemCount() {
      return tags.size();
    }
  }

  private class CardAdapter extends BaseAdapter {

    Vector<Card> items;

    CardAdapter() {
      this.items = new Vector<>();
    }

    public void add(Card item) {
      items.add(item);
    }

    @Override
    public int getCount() {
      return items.size();
    }

    @Override
    public Object getItem(int position) {
      return items.get(position);
    }

    @Override
    public long getItemId(int position) {
      return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // TODO add some recycling
      LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      convertView = li.inflate(R.layout.cardlayout, parent, false);

      View finalConvertView = convertView;

      // set the title
      TextView title = finalConvertView.findViewById(R.id.cardtextViewtitle);
      title.setText(items.get(position).title);

      // set the tags
      LinearLayoutManager layoutManager = new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false);
      RecyclerView tags = finalConvertView.findViewById(R.id.cardTags);
      tags.setLayoutManager(layoutManager);
      tags.setAdapter(new TagAdapter(items.get(position).tags));

      ImageRequest imageRequest = new ImageRequest(items.get(position).media_url, (bitmap) -> {
        Log.i("Jackr", "Received bitmap");
        ImageView imageView = finalConvertView.findViewById(R.id.cardimageView);

        float aspectRatio = bitmap.getWidth() /
            (float) bitmap.getHeight();

        if (aspectRatio > 1) {
          int width = imageView.getWidth();
          Log.i("MainActivity", "width: " + width);
          int height = Math.round(width / aspectRatio);

          bitmap = Bitmap.createScaledBitmap(
              bitmap, width, height, false);
        } else {
          int height = imageView.getHeight();
          int width = Math.round(height * aspectRatio);

          bitmap = Bitmap.createScaledBitmap(
              bitmap, width, height, false);
        }

        // scale the image to fit the card
        imageView.setImageBitmap(bitmap);
      }, 0, 0, null, null);

      SingletonQueue.getInstance(parent.getContext()).addToRequestQueue(imageRequest);

      return convertView;
    }
  }

  // Searches for images based on the tag
  private void search() {
    ListView list = findViewById(R.id.list);
    list.setAdapter(new CardAdapter());

    SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
    String tag = prefs.getString("tag", "cats");

    new AsyncFlickrJSONData((json) -> {
      try {
        JSONArray items = json.getJSONArray("items");

        CardAdapter adapter = (CardAdapter) list.getAdapter();

        SingletonQueue.getInstance(this.getApplicationContext()).getRequestQueue();

        for (int i = 0; i < items.length(); i++) {
          JSONObject item = items.getJSONObject(i);
          String link = item.getJSONObject("media").getString("m");
          String title = item.getString("title");
          String tags = item.getString("tags");


          Log.i("Jackr", "Received item: " + title);
          Log.i("Jackr", "Received item: " + tags);

          Card card = new Card(link, title);
          card.setTags(tags);

          adapter.add(card);

          Log.i("Jackr", "Adding to adapter url : " + link);
        }

        adapter.notifyDataSetChanged();

      } catch (JSONException e) {
        e.printStackTrace();
      }

    }).execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=" + tag + "&format=json");
  }

  private void openPreferences(View view) {
    Intent intent = new Intent(this, MyPrefActivity.class);
    startActivity(intent);
  }

  // A method for the geo button. Fetches images from the current location
  private void getGeoImages(View view) {
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
      return;
    }

    fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, new CancellationToken() {
      @Override
      public boolean isCancellationRequested() {
        return false;
      }

      @NonNull
      @Override
      public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
        return null;
      }
    })
        .addOnSuccessListener(this, location ->
        {
          // Got last known location. In some rare situations this can be null.
          if (location != null) {
            ListView list = findViewById(R.id.list);
            list.setAdapter(new CardAdapter());

            // Logic to handle location object
            Log.i("Jackr", "Latitude: " + location.getLatitude());
            Log.i("Jackr", "Longitude: " + location.getLongitude());

            String url = "https://api.flickr.com/services/rest/?" +
                "method=flickr.photos.search" +
                "&license=4" +
                "&api_key=2e1c384cee069b92d92b0cdf945fd14c" +
                "&has_geo=1&lat=" + location.getLatitude() +
                "&lon=" + location.getLongitude() + "&extras=url_q,tags&format=json";

            Log.i("Jackr", "URL: " + url);

            new AsyncFlickrJSONData((json) -> {
              try {
                JSONArray items = json.getJSONObject("photos").getJSONArray("photo");

                CardAdapter adapter = (CardAdapter) list.getAdapter();

                SingletonQueue.getInstance(this.getApplicationContext()).getRequestQueue();

                for (int i = 0; i < items.length(); i++) {
                  JSONObject item = items.getJSONObject(i);
                  String link = item.getString("url_q");
                  String title = item.getString("title");
                  String tags = item.getString("tags");


                  Log.i("Jackr", "Received item: " + title);
                  Log.i("Jackr", "Received item: " + tags);

                  Card card = new Card(link, title);
                  card.setTags(tags);

                  adapter.add(card);

                  Log.i("Jackr", "Adding to adapter url : " + link);
                }

                adapter.notifyDataSetChanged();

              } catch (JSONException e) {
                e.printStackTrace();
              }

            }).execute(url);
          }
        });

  }

  @RequiresApi(api = Build.VERSION_CODES.P)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);

    this.listener = (sharedPreferences, key) -> {
      Log.i("Jackr", "Preferences changed");
      if (key.equals("tag")) {
        Log.i("Jackr", "Changed tag" + sharedPreferences.getString(key, "cats"));
        TextView tag = findViewById(R.id.tagView);
        tag.setText(sharedPreferences.getString(key, ""));
        search();
      }

      if (key.equals("cache")) {
        Boolean cache = sharedPreferences.getBoolean(key, true);
        Log.i("Jackr", "Changed cache: " + cache);

        SingletonQueue.getInstance(this).setCache(cache);
      }
    };

    SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);

    search();
    TextView tag = findViewById(R.id.tagView);
    tag.setText(prefs.getString("tag", "cats"));

    prefs.registerOnSharedPreferenceChangeListener(listener);

    Button button = findViewById(R.id.button);
    button.setOnClickListener(this::openPreferences);

    Button button2 = findViewById(R.id.button2);
    button2.setOnClickListener(this::getGeoImages);

    // logs the user's current gps location
    fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      // request permission
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }


  }
}
