# Flickr App

## Final Screenshot

![](./Screenshot_20220529-185750_Flickr.jpg)

## Question 12:  Why the answer of the server is not really using the JSON format ? What should be removed?

The server is answering with a `JSONP`.

`JSONP` is a `JS` function that is called with the `JSON` data.

> JSONP is a method for sending JSON data without worrying about cross-domain issues.
[source](https://www.w3schools.com/js/js_json_jsonp.asp)

To obtain the `JSON` format, we can treat the file as plain text and simply remove
the `jsonFlickrFeed()` wrapper to obtain a `JSON` string.

## Question 13: We propose to develop a class that extends `AsyncTask<String, Void, JSONObject>`. Why choosing such a type ?

> The three types used by an asynchronous task are the following:Params, the type of the parameters sent to the task upon execution. Progress, the type of the progress units published during the background computation. Progress, the type of the progress units published during the background computation. Result, the type of the result of the background computation
[source](https://developer.android.com/reference/android/os/AsyncTask)

Our async task will be able to receive a string as parameter (the tag we are looking for) and will
return a JSONObject, no progress is needed.

